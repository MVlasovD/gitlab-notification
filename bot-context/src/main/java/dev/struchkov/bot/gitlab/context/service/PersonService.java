package dev.struchkov.bot.gitlab.context.service;

import dev.struchkov.bot.gitlab.context.domain.entity.Person;
import dev.struchkov.haiti.context.service.SimpleManagerService;

/**
 * @author upagge 15.01.2021
 */
public interface PersonService extends SimpleManagerService<Person, Long> {

}
