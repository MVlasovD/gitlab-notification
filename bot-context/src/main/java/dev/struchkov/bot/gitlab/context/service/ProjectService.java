package dev.struchkov.bot.gitlab.context.service;

import dev.struchkov.bot.gitlab.context.domain.entity.Project;
import dev.struchkov.haiti.context.service.SimpleManagerService;

/**
 * @author upagge 14.01.2021
 */
public interface ProjectService extends SimpleManagerService<Project, Long> {

}
